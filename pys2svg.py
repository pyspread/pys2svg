#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright Martin Manns
# Distributed under the terms of the GNU General Public License

# --------------------------------------------------------------------
# py2svg is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# py2svg is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with py2svg.  If not, see <http://www.gnu.org/licenses/>.
# --------------------------------------------------------------------

"""
pys2svg
=======

@author: Martin Manns


Command line tool that converts pyspread files in pys or pysu format into
SVG files.

"""

from argparse import ArgumentParser
from pathlib import Path
import sys

from PyQt5.QtWidgets import QApplication

# sys.path.insert(0, "/home/mn/prog/pyspread")  # TODO: Remove for pyspread 2.1
from pyspread.main_window import MainWindow
from pyspread.dialogs import SinglePageArea

APP_NAME = 'pys2svg'
VERSION = "0.0.1"


class PyspreadArgumentParser(ArgumentParser):
    """Parser for the command line"""

    def __init__(self):

        description = "pys2svg is a command line tool that converts pyspread" \
                      " files in pys or pysu format into svg files."

        super().__init__(prog=APP_NAME, description=description)

        self.add_argument('--version', action='version', version=VERSION)

        self.add_argument('pys_file', type=Path,
                          help='pyspread input file in pys or pysu format')

        self.add_argument('svg_file', type=Path, help='svg output file')

        self.add_argument('--force', action='store_true',
                          help='Force conversion of unsigned pys file. '
                               'This overrides the default behavior '
                               'and creates an svg file with cell code '
                               'if `--force_acknowledge` is not set.')

        self.add_argument('--force_acknowledge', action='store_true',
                          help='Force acknowledgement for unsigned pys files.'
                               ' `--force_acknowledge` works without force '
                               'being set.\n'
                               'WARNING: If the pys file is unsigned, then '
                               'it is automatically acknowledged. \n'
                               'THIS MAY EXECUTE MALICIOUS CODE IN THE '
                               'PYS FILE!')

        self.add_argument('--quiet', action='store_true',
                          help='Suppress all user output.')

        group = self.add_argument_group('Export area',
                                        'Area of grid that is exported')

        group.add_argument('--top', type=int, metavar='TOP_ROW', default=0,
                           help='top row for export (default: 0)')
        group.add_argument('--bottom', type=int, metavar='BOTTOM_ROW',
                           default=20,
                           help='bottom row for export (default: 20)')
        group.add_argument('--left', type=int, metavar='LEFT_COLUMN',
                           default=0,
                           help='left column for export (default: 0)')
        group.add_argument('--right', type=int, metavar='RIGHT_COLUMN',
                           default=10,
                           help='right column for export (default: 10)')
        group.add_argument('--table', type=int, default=0,
                           help='table for export (default: 0)')


class Pys2Svg:
    """Main program"""

    def __init__(self, args):
        self.args = args

        self._exit_on_invalid_infile()

        self.app = QApplication(sys.argv)

        self.load_pys()
        self.handle_safe_mode()
        self.write_svg()

    def print(self, *args, **kwargs):
        """Prints if quiet option is not set"""

        if not self.args.quiet:
            print(*args, **kwargs)

    def exit(self, msg):
        """System exit with message only if quiet option is not set"""

        if self.args.quiet:
            raise SystemExit()
        raise SystemExit(msg)

    def _exit_on_invalid_infile(self):
        """Checks pys file and exits program if not given or not present"""

        if not self.args.pys_file.is_file():
            self.exit(f"`{self.args.pys_file}` is not a file.")

    def load_pys(self):
        """Load pys file inside `MainWindow`"""

        self.main_window = MainWindow(self.args.pys_file)

    def handle_safe_mode(self):
        """Handles safe mode depending on args

        Exits if `pys_file` is unsigned and conversion is not forced

        """

        if self.main_window.safe_mode:
            self.print(f"Warning: `{self.args.pys_file}` is unsigned.")

            if not self.args.force and not self.args.force_acknowledge:
                self.exit("Stopping conversion.")

            if self.args.force_acknowledge:
                self.print("FORCING ACKNOWLEDGEMENT.")
                self.main_window.safe_mode = False
            else:
                self.print("Cells contain cell code and not results.")

    def write_svg(self):
        """Write svg file"""

        svg_export = self.main_window.workflows.svg_export
        svg_area = SinglePageArea(self.args.top, self.args.left,
                                  self.args.bottom, self.args.right)
        filepath = self.args.svg_file

        svg_export(filepath, svg_area)


def main():
    """Parse args and start main program"""

    parser = PyspreadArgumentParser()
    args, _ = parser.parse_known_args()
    Pys2Svg(args)


if __name__ == '__main__':
    main()
