# pys2svg

[![pyspread community board](https://badges.gitter.im/pyspread/community.svg)](https://gitter.im/pyspread/community)


**pys2svg** is a command line tool that converts
[pyspread](https://pyspread.gitlab.io/) files in pys or pysu format
into SVG files.

It is released under the [GPL v3. LICENSE](LICENSE)

- Repository: https://gitlab.com/pyspread/py2svg
- API Docs: https://pyspread.gitlab.io/pyspread/


# Installation


```bash
pip install pyspread
```

## Usage

```bash
$ pys2svg --top 0 --bottom 20 --left 1 --right 5 --table 0 <my_pyspread_file.pys> <my_svg_file.svg>
```

## Support

In case of qustions, please contact the [pyspread community board](https://gitter.im/pyspread/community).

