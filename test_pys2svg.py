# -*- coding: utf-8 -*-

# Copyright Martin Manns
# Distributed under the terms of the GNU General Public License

# --------------------------------------------------------------------
# pyspread is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# pyspread is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with pyspread.  If not, see <http://www.gnu.org/licenses/>.
# --------------------------------------------------------------------


"""
test_pys2svg
============

Unit tests for pys2svg.py

""" 

from pathlib import Path

import pytest

from pys2svg import Pys2Svg

class Args:
    quiet = False
    pys_file = Path("invalid.pysu")

class TestPys2Svg:
    args = Args()
    
    def test_pys2svg(self):
        with pytest.raises(SystemExit) as err:
            Pys2Svg(self.args)
            assert e.message == f"`{self.args.pys_file}` is not a file."
        
